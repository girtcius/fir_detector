# fir_detector

fir detection test model based on tensorflow object detection api

usage:

- clone repo
- install [dependencies](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/installation.md)
- run jupyter notebook